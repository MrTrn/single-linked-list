/*
 * DA-ALG1000, Oblig 1,2014
 * Terje Rene E. Nilsen - terje.nilsen@student.hive.no
 */

package tenilsen.oblig1;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author Terje Rene E. Nilsen
 */
public class TenilsenOblig1 {
    private static Scanner scannerInput;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)  {
            // application magic here
            System.out.println("DA-ALG1000 - Oblig 1");
            System.out.println("Terje Rene E. Nilsen - terje.nilsen@student.hive.no");
            printMenu(); // prints the menu
            SingelLinkedList myList = new SingelLinkedList(); // our list
            Node myNode;
            int userChoice; // holding integer input from the user 
            int tempInt; // used for holding temp values inside cases
            boolean programRunning = true;
            
            while (programRunning) {
                try {
                    System.out.print(":");
                    scannerInput = new Scanner(System.in);  
                    userChoice = scannerInput.nextInt();
			
                    
                    switch (userChoice) {
                        case 0: // Show menu
                            printMenu();
                            break;
                        case 1: // Delete the first item
                            System.out.print("Delete the first item: ");
                            if (myList.isEmpty()) {
                                System.out.print("List is already empty!\n");
                            }
                            else {
                                myList.deleteFirstElement();
                                System.out.print("Done!\n");
                            }
                            break;
                        case 2: // Delete last item
                            System.out.print("Delete last item: ");
                            if (myList.isEmpty()) {
                                System.out.print("List is already empty!\n");
                            }
                            else {
                                myList.deleteLastElement();
                                System.out.print("Done!\n");
                            }
                            break;
                        case 3: // Remove the first element with the specified value
                            if (myList.isEmpty()) {
                                System.out.println("List is already empty!");
                            }
                            else {
                                System.out.print("Enter item to delete: ");
                                scannerInput = new Scanner(System.in);  
                                tempInt = scannerInput.nextInt();
                                if (myList.isElementInList(tempInt)) {
                                    myList.deleteElementOfValue(tempInt);
                                    System.out.print("Done!\n");
                                }
                                else {
                                    System.out.println("No such element: " + tempInt + "!");
                                }
                            }
                            break;
                        case 4: // Add element at beginning of list
                            System.out.print("Add element at beginning of list: ");
                            scannerInput = new Scanner(System.in);  
                            userChoice = scannerInput.nextInt();
                            myNode = new Node(userChoice);
                            myList.insertInFront(myNode);
                            System.out.println("Done!");
                            break;
                        case 5: // Add element at end of list
                            System.out.print("Add element at end of list: ");
                            scannerInput = new Scanner(System.in);  
                            userChoice = scannerInput.nextInt();
                            myNode = new Node(userChoice);
                            myList.insertAtEnd(myNode);
                            System.out.println("Done!");
                            break;
                        case 6: // Add an element after the first element with given value
                            System.out.print("Add an element after the first element with THIS value: ");
                            scannerInput = new Scanner(System.in);  
                            tempInt = scannerInput.nextInt();
                            if (myList.isElementInList(tempInt)) {
                                System.out.print("Enter value of NEW element: ");
                                scannerInput = new Scanner(System.in);  
                                userChoice = scannerInput.nextInt();
                                myNode = new Node(userChoice);
                                myList.insertAfterElementOfValue(myNode, tempInt);
                                System.out.println("Done!");
                            }
                            else {
                                System.out.println("No such element: " + tempInt + "!");
                            }
                            break;
                        case 7: // Add an element before the first element with given value
                            System.out.print("Add an element before the first element with THIS value: ");
                            scannerInput = new Scanner(System.in);  
                            tempInt = scannerInput.nextInt();
                            if (myList.isElementInList(tempInt)) {
                                System.out.print("\nEnter value of NEW element: ");
                                scannerInput = new Scanner(System.in);  
                                userChoice = scannerInput.nextInt();
                                myNode = new Node(userChoice);
                                myList.insertBeforeElementOfValue(myNode, tempInt);
                                System.out.println("Done!");
                            }
                            else {
                                System.out.println("No such element: " + tempInt + "!");
                            }
                            break;
                        case 8: // Print number of elements
                            System.out.println("Lenght of list: " + myList.getLenght() + " element(s)");
                            break;
                        case 9: // Print number of elements of a given value
                            if (myList.isEmpty()) {
                                System.out.println("List is empty!");
                            }
                            else {
                                System.out.print("Count the number of elements with THIS value: ");
                                scannerInput = new Scanner(System.in);  
                                userChoice = scannerInput.nextInt();
                                System.out.println("Number of elements " + myList.getNumberOfElementsOfValue(userChoice));
                            }
                            break;
                        case 10: // Print list with new line after every 5 elements
                            System.out.println(myList.toStringSpecial());
                            break;
                        case 11: // Delete the list
                            if (myList.isEmpty()) {
                                System.out.println("List is already empty!\n");
                            }
                            else {
                                tempInt = myList.getLenght();
                                myList.deleteAll();
                                System.out.println("Done!\n" + tempInt + " element(s) was deleted.");
                            }
                            break;
                        case 12: // Add numbers using insertInFront() insertAtEnd() (for testing)
                            System.out.print("Adding numbers: ");
                            myNode = new Node(4);
                                myList.insertInFront(myNode);
                                System.out.print("4 in front, ");
                            myNode = new Node(5);
                                myList.insertAtEnd(myNode);
                                System.out.print("5 at end, ");
                            myNode = new Node(3);
                                myList.insertInFront(myNode);
                                System.out.print("3 in front, ");
                            myNode = new Node(6);
                                myList.insertAtEnd(myNode);
                                System.out.print("6 at end.\n");
                            break;
                        case 13: // User wants to exit.
                            scannerInput.close(); programRunning = false;
                            break;
                        default: // Wrong choice
                            System.out.println("No such choice. " + "Enter a value from 0-13");
                            break;
                    }
                }
                catch(InputMismatchException | NumberFormatException e){
                    if ("exit".equals(scannerInput.next())) { 
                        // typing exit will exit the program
                        // (old habits etc..)
                        scannerInput.close(); programRunning = false; 
                    }
                    else {
                        // User entered something different from an integer
                        System.out.println("Input error: " + "Only integers are accepted \n" +
                                "No changes were made.\n\n"+
                            "Enter 0 for menu, 13 for exit.");
                    }
                }
                /*catch(NullPointerException e){
                    // Bad programming.. For debugging..
                    System.out.println("NullPointerException! Avoided crash :)");
                    
                }//*/
            } // while(true) end
    }
    private static void printMenu() {
        System.out.println("");
        System.out.println("Menu:");
        System.out.println("0: Show menu. \n" +
"1: Delete the first item in the list. \n" +
"2: Delete the last item in the list. \n" +
"3: Remove an element with the specified value from the list\n   (removing only the first occurrence). \n" +
"4: Add an element with the specified value at the beginning of the list. \n" +
"5: Add an element with the specified value at the end of the list. \n" +
"6: Add an element after the first element of a specified value. \n" +
"7: Add an element before the first element of a specified value. \n" +
"8: Print the length of the list. \n" +
"9: Count the number of occurrences of the element with the given\n   value in the list, this number is printed. \n" +
"10: Print the list. A maximum of 5 items per line. \n" +
"11: Delete the entire list. How many items that were deleted are printed. \n" +
"12: Add four numbers to the list using insertInFront() and insertAtEnd(). \n" + 
"13: Exit"                );   
    }
}
