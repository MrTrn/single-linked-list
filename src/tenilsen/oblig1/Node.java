/**
 * Node.java
 * 
 * DA-ALG1000, Oblig 1, 2014
 * Terje Rene E. Nilsen - terje.nilsen@student.hive.no
 */

package tenilsen.oblig1;

/**
 * 
 * @author Terje Rene E. Nilsen
 */
public class Node {
    int element;
    Node next;
    
    public Node() {
         element = 0;
         next = null;
    }
    
    public Node(int i) {
         element = i ;
         next = null;
    }
    
    public Node(int i, Node n) {
         element = i ;
         next = n;
    }

    
    
}
