/**
 * SingelLinkedList.java
 * 
 * DA-ALG1000, Oblig 1, 2014
 * Terje Rene E. Nilsen - terje.nilsen@student.hive.no
 */

package tenilsen.oblig1;

/**
 *
 * @author Terje Rene E. Nilsen
 * Oppgaven:
1:+	Slett det første elementet i listen.
2:+	Slett det siste elementet i listen.
3:+	Slett et element med oppgitt verdi fra listen (slett kun første forekomst).
4:+	Legg til et element med oppgitt verdi i starten av listen.
5:+	Legg til et element med oppgitt verdi i slutten av listen.
6:+	Legg til et element etter et element med oppgitt verdi (gjøres én gang).
7:+	Legg til et element foran et element med oppgitt verdi (gjøres én gang).
8:+	Skriv ut lengden på listen.
9:+	Tell opp antall forekomster av element  med gitt verdi i lista, dette 
        antallet skrives ut.
10:+	Skriv ut hele listen. Maks 5 elementer pr. linje.
11:+	Slett hele listen. Hvor mange elementer som ble slettet, skrives ut.
 * 
 */
public class SingelLinkedList {
    private Node head;
    private int elements;
    
    public SingelLinkedList() {
        head = null;
        elements = 0;
    }
    
    private void insertFirst(Node n) {
        head = n; 
        elements++;
    }    
    
    public void insertInFront(Node n) {
        
        n.next = head;
        head = n; 
        elements++;
        
    }
    
    public boolean isEmpty() {
        return (head == null);
    }
    
    public void insertAtEnd (Node n) {
        if (elements == 0) {
            insertFirst(n); // Special case
        }
        else if (elements == 1) {
            head.next = n; // Special case
            elements++;           
        }
        else {
            Node lastNode;
            lastNode = secondLastNode().next;
            lastNode.next = n;
            elements++;
        }    
    }
    
    // "insert before first element of value "
    public void insertBeforeElementOfValue (Node n, int value) {
        if (elements == 0) {  
              insertFirst(n); // Special case
        }
        else {
            Node lastNode = findElementOfValueSpecial(value);
            n.next = lastNode.next;
            lastNode.next = n;
            elements++;
        }
    }
    
    // "insert after first element of value"
    public void insertAfterElementOfValue (Node n, int value) {
       if (elements == 0) {  
            insertFirst(n); // Special case
       }
       else {
            Node lastNode = findElementOfValueSpecial(value).next;
            n.next = lastNode.next;
            lastNode.next = n;
            elements++;
       }
    }
    
    public void deleteFirstElement() {
        head = head.next;
        elements--;
    }
    
    public void deleteLastElement() {
        if (elements == 1) {
            deleteFirstElement();   // Last element is the only element
        }
        else{
            Node n = secondLastNode();
            n.next = null;
            elements--;
        }
    }
    
    public void deleteElementOfValue(int value) {
        if (head.next.element == value) {
            head.next = head.next.next;
        }
        else {
            Node lastNode = findElementOfValueSpecial(value);
            lastNode.next = lastNode.next.next;
        }
        elements--;
    }    
    
    /**
     * 
     */
    public void deleteAll() {
        head = null;
        elements = 0;
    }
    
   /** 
    * toStringSpecial()
    * 
    * @return String med maks 5 elementer per linje
    */
    public String toStringSpecial() {
        
        if (!isEmpty()) {
    
            String returnString = "";
            Node n = head;
            int counter = 1, cunt = 1;
            while (cunt<=elements) { 
                returnString += n.element + " "; // + "(" + cunt + ") "; // debug
                if (counter == 5) { returnString += "\n"; counter = 0; }
                n = n.next;
                cunt++;
                counter++;
            }

            return returnString;
        }
        else {
            return "(List is empty!)";
        }
    }
    
    /**
     * 
     * @return 
     * 
     * Elements(nodes):
     * [7]--[5]--[2]--[8]--[3]
     * secondLastNode() returns [8]
     * [7]
     * secondLastNode() returns [head]
     */
    private Node secondLastNode() {
        // Special case: element == 1
        if (elements == 1) {
            return head;
        }
        else {
            Node n = head;
            while (n.next.next != null) {
                n = n.next;
            }
            return n;
        }
    }
    
    public int getLenght() {
        return elements;
    }
    
    public int getNumberOfElementsOfValue(int value) {
        int counter = 0;
        Node n = head;
        for (int i = 1; i <= elements; i++) {
            if (n.element == value)
                counter++;
            n = n.next;
        }
        return counter;        
    }
    
    /**
     * 
     * @param value
     * @return ElementOfValue - 1, null if not found
     * 
     * Elements(nodes):
     * [7]--[5]--[2]--[8]--[3]
     * findElementOfValueSpecial(3) returns [8]
     */
    private Node findElementOfValueSpecial(int value) {
        Node n = head;
        while(n.next.element != value) {
            // if (n.next == null) return null; // add error check when using function..
            n = n.next;
        }
        return n;
    }
    
    public boolean isElementInList(int value) {
        Node n = head;
        while(n != null) {
            if(n.element == value) {
                return true;
            }
            n = n.next;
        }
        return false;
    }

   
}
